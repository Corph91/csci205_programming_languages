#ifndef PLAYER_H
#define PLAYER_H

#include <vector>
#include <string>
#include "Card.h"

class Player{
  public:
    vector<Card> hand;
    int score;
    bool bust;
    void setScore(vector<Card> hand);
    int getScore();
    bool player;
    bool dealer;
};

#endif
