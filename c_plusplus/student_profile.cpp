#include <iostream>
#include <vector>
#include <string>
using namespace std;
using std::vector;
using std::getline;

class Student{
   private:
   int rollno;
   string name;
   int alg;
   int datastruct;
   int architect;
   int prolang;
   float per;
   char grade;
   public:
   void getdata(int, string, int, int, int, int);
   void showdata();
   Student();
};

void Student::getdata(int rollNo, string studentName, int algGrade, int dataGrade, int archGrade, int progGrade){
   rollno = rollNo;
   name = studentName;
   alg = algGrade;
   datastruct = dataGrade;
   architect = archGrade;
   prolang = progGrade;
   per = (algGrade + dataGrade + archGrade + progGrade)/(float)4;
   
   if(per >= 90){
      grade = 'A';
   }
   else if(80<= per && per < 90){
      grade = 'B';
   }
   else if(70<= per && per < 80){
      grade = 'C';
   }
   else if(60<= per && per < 70){
      grade = 'D';
   }
   else if(per < 60){
      grade = 'F';
   }
   return;
}

void Student::showdata(){
   cout<<"Roll number of student: "<<rollno<<endl;
   cout<<"Name of student: "<<name<<endl;
   cout<<"Grade in Algorithms: "<<alg<<endl;
   cout<<"Grade in Data Structures: "<<datastruct<<endl;
   cout<<"Grade in Architecture: "<<architect<<endl;
   cout<<"Grade in Programming Languages: "<<prolang<<endl;
   cout<<"Percentage of student is: "<<per<<endl;
   cout<<"Grade of student is: "<<grade<<endl;
}

Student::Student(){
   rollno = 0;
   name = "NoName";
   alg = 0;
   datastruct = 0;
   architect = 0;
   prolang = 0;
   per = 0.0;
   grade = 'F';
   
}

int main() {
   bool run = true;
   
   while(run){
      int size;
      /* Type your code here. */
      cout<<"Enter size of class: ";
      cin>>size;
      cout<<endl;
      vector<Student> myStudents(size, Student());
      if(size < 1){
         cout<<"Invalid class size"<<endl;
         continue;
      }
      
      for(int i=0; i<size; i++){
         int rollNo = 0;
         string studentName = "";
         int algGrade = 0;
         int dataGrade = 0;
         int archGrade = 0;
         int progGrade = 0;
         
         cout<<"Enter the roll number of student: ";
         cin>>rollNo;
         cin.ignore();
         cout<<endl;
         cout<<"Enter The Name of student: ";
         getline(cin,studentName);
         cout<<endl;
         cout<<"Enter the grade in Algorithms out of 100: ";
         cin>>algGrade;
         cin.ignore();
         cout<<endl;
         cout<<"Enter the grade in Data Structures out of 100: ";
         cin>>dataGrade;
         cin.ignore();
         cout<<endl;
         cout<<"Enter the grade in Architecture out of 100: ";
         cin>>archGrade;
         cin.ignore();
         cout<<endl;
         cout<<"Enter the grade in Programming Languages out of 100: ";
         cin>>progGrade;
         cin.ignore();
         cout<<endl;
         
         if(studentName.length() > 0 && rollNo > 0 && algGrade > 0 && dataGrade > 0 && progGrade > 0){
            myStudents[i].getdata(rollNo, studentName, algGrade, dataGrade, archGrade, progGrade);
         }
         
      }
      
      for(int i=0; i<size; i++){
         myStudents[i].showdata();   
      }
      return 0;
   }
}
