int main(){

    const int NUM_ITEMS = 2;
    char c;
    int total = 0;
    ItemToPurchase item1;
    ItemToPurchase item2;

    for(int i=0; i<NUM_ITEMS; i++){
	if(i==0){
	    printf("Item %d\n",i+1);
	    printf("Enter the item name:\n");
	    fgets((item1.itemName),50,stdin);
	    printf("Enter the item price:\n");
	    scanf("%d", &(item1.itemPrice));
	    printf("Enter the item quantity:\n");
	    scanf("%d",&(item1.itemQuantity));
	}
	if(i==1){
	    printf("Item %d\n",i+1);
	    printf("Enter the item name:\n");
	    fgets((item2.itemName),50,stdin);
	    printf("Enter the item price:\n");
	    scanf("%d", &(item2.itemPrice));
	    printf("Enter the item quantity:\n");
	    scanf("%d",&(item2.itemQuantity));
	}
	while ((c = getchar()) != EOF && c != '\n');
    }

    total = ((item1.itemPrice * item1.itemQuantity) + (item2.itemPrice * item2.itemQuantity));
    printf("TOTAL COST\n");
    printf("\n%s %d @ $%d = $%d", item1.itemName, item1.itemQuantity, item1.itemPrice, item1.itemPrice * item1.itemQuantity);
    printf("\n%s %d @ $%d = $%d", item2.itemName, item2.itemQuantity, item2.itemPrice, item2.itemPrice * item2.itemQuantity);
    printf("\nTotal: $%d\n",total);

    return 0;
}


