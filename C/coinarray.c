#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    
    srand((int)time(0));
    int flips;
    int coin;
    int array [11];
    int pos = 5;

    printf("Enter the number of coin tosses you want:\n");
    scanf("%d", &flips);
    	
    for(int i=0; i<11; i++){
	array[i] = 0;
    }
    
    for(int i=0; i<flips; i++){
        printf("\nFlipping coin:\n");
	coin = rand()%2;
	if(coin == 1){
	    printf("It's heads!\n");
            pos = (pos + 1)%11;
        }
	else{
	    printf("It's tails!\n");
	    pos = (pos - 1)%11;
	}
	
	array[pos] += 1;
    }

    printf("\nFinal result:\n");
    for(int i=0; i<11; i++){
	printf("Value at pos. %d is: %d ", i, array[i]);
	for(int j=1; j<=array[i]; j++){
	    printf("*");	
	}
        printf("\n");
    }

    return 0;
}
