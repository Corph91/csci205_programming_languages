#include <stdio.h>
#include <string.h>
#include "ShoppingCart.h"

ShoppingCart AddItem(ItemToPurchase item, ShoppingCart *cart){
        cart->cartSize = cart->cartSize + 1;
	for(int i=0; i<cart->cartSize; i++){
	    if(cart->cartItems[i].itemPrice == 0 || (strcmp(cart->cartItems[i].itemName, "none") == 0)){
		cart->cartItems[i] = item;
	    }
	}
}

ShoppingCart RemoveItem(char name[], ShoppingCart *cart){
	int found = 0;
	for(int i=0; i<cart->cartSize; i++){
	    if(strcmp(cart->cartItems[i].itemName, name) == 0){
		MakeItemBlank(&cart->cartItems[i]);
		for(int j=i+1; j<cart->cartSize; j++){
		    cart->cartItems[j-1] = cart->cartItems[j];
		}
		cart->cartSize = cart->cartSize - 1;
		found = 1;
		break;
	    }
	}
	if(found == 0){
	    printf("Item not found in cart. Nothing removed.\n");
	}
}

ShoppingCart ModifyItem(ItemToPurchase item, ShoppingCart *cart){
    int found = 0;
    for(int i=0; i<cart->cartSize; i++){
	if(strcmp(item.itemName, cart->cartItems[i].itemName) == 0){
	    item.itemPrice = cart->cartItems[i].itemPrice;
	    strcpy(cart->cartItems[i].itemDescription,item.itemDescription);
            cart->cartItems[i] = item;
	    found = 1;
	    break;
	}
    }
    if(found == 0){
	printf("Item not found in cart. Nothing modified.\n");
    }
}

int GetNumItemsInCart(ShoppingCart *cart){

    int numItems = 0;    

    for(int i=0; i<cart->cartSize; i++){
	numItems += cart->cartItems[i].itemQuantity;
    }
    return numItems;
}

int GetCostOfCart(ShoppingCart *cart){

    int total = 0;
    for(int i=0; i<cart->cartSize; i++){
	total += (cart->cartItems[i].itemPrice * cart->cartItems[i].itemQuantity);
    }
   
    return total;
}

void PrintTotal(ShoppingCart *cart){
    if(cart->cartSize == 0){
	printf("Number of Items: %d\n\n",GetNumItemsInCart(cart));
	printf("SHOPPING CART IS EMPTY\n");
	printf("\nTotal: $0");
    }
    else{
	printf("Number of Items: %d\n\n",GetNumItemsInCart(cart));
        for(int i=0; i<cart->cartSize; i++){
          PrintItemCost(cart->cartItems[i]);
        }
	printf("\nTotal: $%d", GetCostOfCart(cart));
    }

}

void PrintDescriptions(ShoppingCart *cart){
    printf("Item Descriptions\n");
    for(int i=0; i<cart->cartSize; i++){
	printf("%s: %s\n",cart->cartItems[i].itemName,cart->cartItems[i].itemDescription);
    }
}

