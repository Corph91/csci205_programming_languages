/*
 * Sean Corbett
 * 02/07/2017
 * CSCI205: Programming Languages
 * Project 1: Slots in C
*/


/* 
 * C doesn't include the Boolean type by
 * default, so I imported the bool type
 * from the standard library.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// Main function runs whole program.
int main(){

    // Init vars, including random. Array is defined but not initialized.
    srand((int)time(0));
    _Bool run = true;
    char userInput[1];
    int size = 3;
    int slots[size];
    int score;

    // While we want to run.
    while(run){
    
	// Ask user if they would like to play. Will ask each iteration
	// of while loop until user enters "n" (for "no").
        printf("Would you like to play? (y/n)\n");
        scanf("%s", userInput);

	// If user wants to play.
        if(strcmp(userInput,"y") == 0){

	    // Initialize score to 0 each time.
 	    printf("\nPull the level Kronk!\n\n");
	    score = 0;


	    // Fill slots array with random integers range 0-9 (inclusive).
	    for(int i=0; i<size; i++){
	    	slots[i] = (rand()%10);	
	    }
            
	    // Print out each slot's number.
	    for(int i=1; i<=size; i++){
		printf("Slot %d is: %d\n",i,slots[i-1]);
	    }


	    /* 
	     * If the integers for all three slots are equal, we have hit 
	     * jackpot. Else if two integers of the three are equal, we still
	     * win but not jackpot. Finally, if no integers are equal, we bust.
	    */
	    if(slots[0] == slots[1] && slots[0] == slots[2] && slots[1] == slots[2]){
		score = 3;
		printf("\n%d slots match! Jackpot!\n\n", score);
	    }
	    else if(slots[0] == slots[1] || slots[0] == slots[2] || slots[1] == slots[2]){
		score = 2;
		printf("\n%d slots match! Nice job!\n\n", score);
	    }
	    else{
		printf("\n%d slots match! Oh no! You busted!\n\n", score);
	    }

        }
	else if(strcmp(userInput, "n") == 0){

	    // When user is done, thank for playing and exit loop.
	    printf("\nThank you for playing!\n");
	    run = false;
	
	}
	else{

	    // Input error tolerance is important. This is to account for 
	    // such error.	
	    printf("\nWhoops! Just enter 'y' to play or 'n' to quit!\n\n");
	
	}

    }
    

    return 0;
}
