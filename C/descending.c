#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int cmpfunc(const void* a, const void* b){
    
    return ( *(int*)b - *(int*)a );

}

int main() {
   int size;
   _Bool run = true;
   /* Type your code here. */
    while(run){
	
	printf("Enter the size of an array that can hold up to 30 integers:\n");
	scanf("%d",&size);

	if(size <=30){
	int arry[size];

	printf("Enter the elements of the array:\n");
	for(int i=0; i<size; i++){
	printf("Enter element %d:\n", i+1);
	scanf("%d", &arry[i]);
	}

	qsort(arry,size,sizeof(int),cmpfunc);

	printf("Sorted list is:\n");
	for(int i=0; i<size; i++){
	    printf("Element %d is %d\n",i,arry[i]);	
	}


	break;

	}
	else{

	printf("Error: Enter a smaller size\n");
	break;

	}

    }
    return 0;
}
