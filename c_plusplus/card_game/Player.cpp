#include <vector>
#include <string>
#include "Card.h"
#include "Player.h"

void Player::setScore(vector<Card> hand){
  this->score = 0;
  for(int i=0; i<hand.size(); i++){
    this->score += hand.at(i).value;
  }
}

int Player::getScore(){
 return this->score;
}
