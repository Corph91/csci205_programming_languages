#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <numeric>

using namespace std;

void GetNumOfNonWSCharacters(string usrStr) {
    int i;
    int count = 0;

    for(i=0; i<(int)usrStr.length(); i++){
	if(usrStr[i] != ' '){
	    count++;
	}
    }

    cout<<"Number of non-whitespace characters: "<<count<<endl<<endl;
    return;
}

void GetNumOfWords(string usrStr) {
    int count = 0;
    std::stringstream stream(usrStr);
    count = std::distance(std::istream_iterator<string>(stream), std::istream_iterator<string>());
   
    cout<<"Number of words: "<<count<<endl<<endl;
}

void FindText(string usrStr) {
    string str;
    cout<<"Enter a word or phrase to be found:"<<endl;
    cin.ignore();
    getline(cin,str);

    int count = 0;
    size_t pos=0;
    while ((pos = usrStr.find(str, pos)) != std::string::npos) {
        ++count;
        ++pos;
    }

    cout<<'"'<<str<<'"'<<" instances: "<<count<<endl<<endl;
    return;
}

string ReplaceExclamation(string usrStr) {
    size_t pos=0;
    while ((pos = usrStr.find('!', pos)) != std::string::npos) {
      usrStr[pos] = '.'; 
      ++pos;
    }
    
    return usrStr;

}


void ShortenSpace(string usrStr) {
   for(int i=0; i<(int)usrStr.length(); i++){
      int count = 0;
      if(usrStr[i] == ' ' && usrStr[i+1] == ' '){
         int j=i+1;
         while(usrStr[j] == ' '){
            count++;
            j++;
         }
         usrStr.erase(i+1,count);
      }
   }
   
   cout<<"Edited text: "<<usrStr<<endl<<endl;
   return;
}


char PrintMenu(string usrStr) {
   char menuOp = ' ';
   cout<<"MENU"<<endl;
   cout<<"c - Number of non-whitespace characters"<<endl;
   cout<<"w - Number of words"<<endl;
   cout<<"f - Find text"<<endl;
   cout<<"r - Replace all !\'s"<<endl;
   cout<<"s - Shorten spaces"<<endl;
   cout<<"q - Quit"<<endl<<endl;
   
   while (menuOp != 'c' && menuOp != 'w' && menuOp != 'f' &&
         menuOp != 'r' && menuOp != 's' && menuOp != 'o' && 
         menuOp != 'q') {
      cout<<"Choose an option:"<<endl;
      cin>>menuOp;
   }
   
   if (menuOp == 'c') {
      GetNumOfNonWSCharacters(usrStr);
      menuOp = ' ';
   }
   
   else if (menuOp == 'w') {
      GetNumOfWords(usrStr);
      menuOp = ' ';
   }
   
   else if (menuOp == 'f') {
      FindText(usrStr);
      menuOp = ' ';
   }
   
   else if (menuOp == 'r') {
      usrStr = ReplaceExclamation(usrStr);
      menuOp = ' ';
      cout<<"Edited text: "<<usrStr<<endl<<endl;
   }
   
   else if (menuOp == 's') {
      ShortenSpace(usrStr);
      menuOp = ' ';
   }
   
   return menuOp;
}

int main() {
   string userString;
   char menuChoice = ' ';

   cout<<"Enter a sample text:"<<endl;
   getline(cin,userString);
   cout<<endl;
   cout<<"You entered: "<<userString<<endl<<endl;
   
   while (menuChoice != 'q') {
      menuChoice = PrintMenu(userString);
   }
   
   return 0;
}


