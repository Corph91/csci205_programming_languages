#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int enterData(char** strings, int *points, int count);
void printTable(char **strings, int *points, char *title, char *col1, char *col2, int numEntries);
void printHistogram(char **strings, int *points,int numEntries);

int main(){
	
    char dataTitle[100] = "";
    char col1[100] = "";
    char col2[100] = "";
    char **strings = malloc(1 * sizeof(char*));
    int* points = malloc(1 * sizeof(int));
    int count = 0;
    int returnVal = 1;

    printf("Enter a title for the data:\n");
    fgets(dataTitle,100,stdin);
    printf("You entered: %s\n",dataTitle);
    printf("Enter the column 1 header:\n");
    fgets(col1,100,stdin);
    printf("You entered: %s\n",col1);
    printf("Enter the column 2 header:\n");    
    fgets(col2,100,stdin);
    printf("You entered: %s\n",col2);
    while(returnVal){
   	strings = realloc(strings, (count + 1) * sizeof(dataTitle));
   	points = realloc(points, (count + 1) * sizeof(int));
   	returnVal = enterData(strings, points, count);
	if(returnVal < 2){
	    count++;
	}
   	
   	
    }
	
    count = count - 1;    
    printTable(strings,points,dataTitle,col1,col2,count);
    printf("\n");
    printHistogram(strings,points,count);
 
    free(strings);
    free(points);

    return 0;
}


int enterData(char **strings, int *points, int count){
    strings[count] = malloc(100 * sizeof(char));
    char dataPoint[100] = "";
    char dataString[100] = "";
    char dataInt[100] = "";
    int intCount = 0;

   dataPoint[strlen(dataPoint)-1] = '\0';   
  
    printf("Enter a data point (-1 to stop input):\n");
    fgets(dataPoint,100,stdin);
    if(atoi(&dataPoint[0]) == -1){
	return 0;
    }
    else{
	int split = 0;
	for(int i=0; i<strlen(dataPoint); i++){
	    if(dataPoint[i] == ','){
	        split += 1;
	    }
	    if(!split){
	       dataString[i] = dataPoint[i];
	    }
	    else if(split == 1 && dataPoint[i] != ','){
	        dataInt[intCount] = dataPoint[i];
		intCount++;
	    }
	}
        if(!split){
	    printf("Error: No comma in string.\n");
	    printf("\n");
	    return 2;
	}
	else if(split > 1){
	    printf("Error: Too many commas in input.\n");
	    printf("\n");
	    return 2;
	}
	else if(split == 1){
	    for(int i=0; i<strlen(dataInt)-1; i++){
		     if((dataInt[i] < '0' || dataInt[i] > '9') && (!isspace(dataInt[i]))){
		         printf("Error: Comma not followed by an integer.\n");
		         printf("\n");
	    	      return 2;
		     }
	    }
	}
        printf("Data string: %s\n",dataString);
	printf("Data integer:%s\n", dataInt);
	printf("\n");
	strcpy(strings[count],dataString);
	points[count] = atoi(dataInt);
    }
    
    return 1;  
}

void printTable(char **strings, int *points, char *title, char *col1, char *col2, int numEntries){
    printf("There are %d entires\n",numEntries);
    int titleLen = strlen(title);
    int col1Len = strlen(col1);
    int col2Len = strlen(col2);
    int just = 0;
    int i;
    int j;
    title[titleLen-1] = '\0';
    col1[col1Len-1] = '\0';
    col2[col2Len-1] = '\0';
    printf("Length of title: %d\n",titleLen);
    just = ((33-titleLen)/2) + 1;
    char snum[100];

    for(i=0; i<=just+3; i++){
	printf(" ");
    }
    printf("%s",title);
    if(just%2 == 0){
	for(i=0; i<just-4; i++){
	    printf(" ");
	}
    }
    else{
	for(i=0; i<=just-3+1;i++){
	    printf(" ");
	}
    }
    printf("\n%s",col1);
    for(i=0; i<=20-col1Len; i++){
	printf(" ");
    }
    printf("|");
    for(i=0; i<=23-col2Len; i++){
	printf(" ");
    }
    printf("%s\n",col2);
    for(i=0; i<=43; i++){
	printf("-");
    }
    printf("\n");
    for(i=0; i<numEntries; i++){
	sprintf(snum, "%d", points[i]);
	just = 20 - strlen(strings[i]) -1;
	printf("%s",strings[i]);
	for(j=0;j<=just; j++){
	    printf(" ");
	}
        printf("|");
        just = 23 - strlen(snum);
        for(j=0; j<just; j++){
	    printf(" ");
	}
	printf("%s\n",snum);  
    }



}

void printHistogram(char **strings, int *points,int numEntries){
    int just = 0;
    int i,j;

   for(i=0; i<numEntries; i++){
	just = 20-strlen(strings[i]);

	for(j=0; j<just; j++){
	    printf(" ");
	}
	printf("%s ",strings[i]);
	for(j=0;j<points[i];j++){
	    printf("*");
	}
        printf("\n");
   }

}
