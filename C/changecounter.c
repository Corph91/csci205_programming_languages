#include <stdio.h>

int main(){
    
    int quarters;
    int dimes;
    int nickles;
    int pennies;
    int change;

    printf("Enter number of quarters:\n");
    scanf("%d", &quarters);
    printf("\nEnter number of dimes:\n");
    scanf("%d", &dimes);
    printf("\nEnter number of nickles:\n");
    scanf("%d", &nickles);
    printf("\nEnter number of pennies:\n");
    scanf("%d", &pennies);

    change = (quarters * 25) + (dimes * 10) + (nickles * 5) + pennies;

    printf("\nTotal value is: %d dollars and %d cents.\n", change/100, change%100);

    return 0;
}
