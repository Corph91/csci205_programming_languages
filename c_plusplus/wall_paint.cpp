#include <iostream>
#include <cmath>
using namespace std;

int main(){
    int height;
    int width;
    int area;
    double paintNeeded;
    int sqFeetPerGallon = 350;

    cout<<"Enter wall height (feet):"<<endl;
    cin>>height;
    cout<<"Enter wall width (feet):"<<endl;
    cin>>width;

    area = width * height;    

    cout<<"Wall area: "<<area<<" square feet"<<endl;

    paintNeeded = (double)area/(double)sqFeetPerGallon;

    cout<<"Paint needed: "<<paintNeeded<<" gallons"<<endl;
    cout<<"Cans needed: "<<ceil(paintNeeded)<<" can(s)"<<endl;

    return 0;
}
