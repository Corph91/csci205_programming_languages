/*
 * Sean Corbett
 * 02/10/2017
 * CSCI205: Programming Languages
 * Assignment 6.13: Largest
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Compare function used in system qsort.
int cmpfunc(const void* a, const void* b){
    
    return ( *(int*)b - *(int*)a );

}

int main(){
    
    // Declare and init size var.
    int size;
    printf("Input total number of elements (1 to 100):\n");
    scanf("%d", &size);

    // If number of elements is in range from (1-100) inclusive.
    if((size >= 1) && (size <= 100)){
	
	// Allocate memory equal to size by size of integer.
	int* array = (int *)malloc(size * sizeof(int));    

	// Fill each array with user input number.
	for(int i=0; i<size; i++){
	printf("Enter element %d: ", i+1);
	scanf("%d", (array+i));
	}

	// Repeat back user input.
	printf("\nYou entered: ");
	for(int i=0; i<size; i++){
	printf("%d ",*(array+i));
	}

	// Perform array sort using system qsort func.
	qsort(array,size,sizeof(int),cmpfunc);    

	// Qsort stores in descending order, so largest element is at position
	// zero. Print out this element.
	printf("\n\nThe largest element is element %d at index %d\n\n",*array, 0);

	free(array);

    }
    // If number of elements is not in range from (1-100) inclusive.
    else {
	
	// Print out error message.
	printf("\nInvalid number of elements.\n");
    }

    return 0;
}
