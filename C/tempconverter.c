#include <stdio.h>

int main(){
    const double conversionFac = (double)5/ (double)9;
    const int subtractConst = 32;
    double fahrenheit;
    double converted;
    double celsius;
    
    printf("Enter temperature in Fahrenheit: ");
    scanf("%lf", &fahrenheit);

    converted = ((fahrenheit - subtractConst) * conversionFac);

    printf("%lf degrees Fahrenheit is %lf degrees Celsius.\n",fahrenheit, converted);
    
    return 0;
}
