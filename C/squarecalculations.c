#include <stdio.h>

int main(){
    
    int sideLength;

    printf("Enter side length:\n");
    scanf("%d", &sideLength);
    printf("\nThe perimeter of your square is %d and its area is %d.\n", sideLength * 4, sideLength * sideLength);
    
    return 0;
}
