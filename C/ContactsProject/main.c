#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Contacts.h"

int main(void) {

	char fullName[50] = "";
	char phoneNum[50] = "";
	char temp[50] = "";
	ContactNode* head = NULL;
	ContactNode* next1 = NULL;
	ContactNode* next2 = NULL;
	ContactNode* current = NULL;

	printf("Person 1\n");
	printf("Enter name:\n");
	fgets(fullName, 50, stdin);
	
	fullName[strlen(fullName)-1] = '\0';

	printf("Enter phone number:\n");
	scanf("%s",phoneNum);

	printf("You entered: %s, %s\n\n", fullName, phoneNum);

	head = (ContactNode*)malloc(sizeof(ContactNode));
	CreateContactNode(head, fullName, phoneNum, NULL);

	fgets(temp, 50, stdin);
	printf("Person 2\n");
	printf("Enter name:\n");
	fgets(fullName, 50, stdin);
	
	fullName[strlen(fullName)-1] = '\0';

	printf("Enter phone number:\n");
	scanf("%s",phoneNum);

	printf("You entered: %s, %s\n\n", fullName, phoneNum);

	next1 = (ContactNode*)malloc(sizeof(ContactNode));
	CreateContactNode(next1, fullName, phoneNum, NULL);	
	InsertContactAfter(head, next1);

	fgets(temp, 50, stdin);
	printf("Person 3\n");
	printf("Enter name:\n");
	fgets(fullName, 50, stdin);
	
	fullName[strlen(fullName)-1] = '\0';

	printf("Enter phone number:\n");
	scanf("%s",phoneNum);

	printf("You entered: %s, %s\n\n", fullName, phoneNum);

	next2 = (ContactNode*)malloc(sizeof(ContactNode));
	CreateContactNode(next2, fullName, phoneNum, NULL);	
	InsertContactAfter(next1, next2);

	current = head;
	printf("CONTACT LIST\n");
	while(current != NULL){
		PrintContactNode(current);
		current = GetNextContact(current);
	}

	return 0;
}
