#include <iostream>
#include <cmath>
using namespace std;

int main(){
    int userInteger;
    int squared;
    int cubed;
    int userInt2;

    cout<<"Enter integer:"<<endl;
    cin>>userInteger;
    cout<<"You entered: "<<userInteger<<endl;
    squared = userInteger * userInteger;
    cout<<userInteger<<" squared is "<<squared<<endl;
    cubed = squared * userInteger;
    cout<<"And "<<userInteger<<" cubed is "<<cubed<<endl;
    cout<<"Enter another integer:"<<endl;
    cin>>userInt2;
    cout<<userInteger<<" + "<<userInt2<<" is "<< userInteger + userInt2<<endl;
    cout<<userInteger<<" * "<<userInt2<<" is "<< userInteger * userInt2<<endl;

    return 0;
}
