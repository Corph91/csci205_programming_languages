#include <iostream>
using namespace std;

int main(){
    int userInt;
    double userDoub;
    char userChar;
    string userStr;
    int newInt;

    cout<<"Enter integer:"<<endl;
    cin>>userInt;
    cout<<"Enter double:"<<endl;
    cin>>userDoub;
    cout<<"Enter character:"<<endl;
    cin>>userChar;
    cout<<"Enter string:"<<endl;
    cin>>userStr;
    cout<<userInt<<" "<<userDoub<<" "<<userChar<<" "<<userStr<<endl;
    cout<<userStr<<" "<<userChar<<" "<<userDoub<<" "<<userInt<<endl;
    
    newInt = (int)userDoub;
    cout<<userDoub<<" as a integer is "<<newInt<<endl;

    return 0;
}
