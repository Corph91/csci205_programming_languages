#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ShoppingCart.h"
#include "ItemToPurchase.h"


char PrintMenu(char userName[],char date[], ShoppingCart *cart) {
   char menuOp = ' ';  

   printf("\n\nMENU\n");
   printf("a - Add item to cart\n");
   printf("r - Remove item from cart\n");
   printf("c - Change item quantity\n");
   printf("i - Output items' descriptions\n");
   printf("o - Output shopping cart\n");
   printf("q - Quit\n\n");
   
   while (menuOp != 'a' && menuOp != 'r' && menuOp != 'c' &&
         menuOp != 'i' && menuOp != 'o' && 
         menuOp != 'q') {
      printf("Choose an option:\n");
      scanf(" %c", &menuOp);
   }
   
   if (menuOp == 'a') {
      int c;
      while ((c = getchar()) != '\n' && c != EOF);
      printf("ADD ITEM TO CART\n");
      ItemToPurchase newItem;
      MakeItemBlank(&newItem);
      printf("Enter the item name:\n");
      fgets(newItem.itemName,256,stdin);
      printf("Enter the item description:\n");
      fgets(newItem.itemDescription,256,stdin);
      printf("Enter the item price:\n");
      scanf("%d", &newItem.itemPrice);
      printf("Enter the item quantity:");
      scanf("%d", &newItem.itemQuantity);
      
      newItem.itemName[strlen(newItem.itemName)-1] = '\0';
      newItem.itemDescription[strlen(newItem.itemDescription)-1] = '\0';      

      AddItem(newItem, cart);
      menuOp = ' ';
   }
   
   else if (menuOp == 'r') {
      int c;
      while ((c = getchar()) != '\n' && c != EOF);
      char query[256];
      printf("REMOVE ITEM FROM CART\n");
      printf("Enter name of item to remove:\n");
      fgets(query,256,stdin);
      
      query[strlen(query)-1] = '\0';   

      RemoveItem(query,cart);
      
      menuOp = ' ';
   }
   
   else if (menuOp == 'c') {
      int c;
      while ((c = getchar()) != '\n' && c != EOF);
      char query[256];
      printf("CHANGE ITEM QUANTITY\n");
      printf("Enter the item name:\n");
      fgets(query,256,stdin);

      query[strlen(query)-1] = '\0';
	
      ItemToPurchase modItem;
      strcpy(modItem.itemName,query);

      printf("Enter the new quantity:\n");
      scanf("%d", &modItem.itemQuantity);

      ModifyItem(modItem,cart);

      menuOp = ' ';
   }
   
   else if (menuOp == 'i') {
      printf("OUTPUT ITEMS' DESCRIPTIONS\n");
      printf("%s\'s Shopping Cart - %s\n", userName, date);

      PrintDescriptions(cart);
      menuOp = ' ';
   }
   
   else if (menuOp == 'o') {
      printf("OUTPUT SHOPPING CART\n");
      printf("%s\'s Shopping Cart - %s", userName, date);
      PrintTotal(cart);
      menuOp = ' ';
   }
   
   return menuOp;
}

int main() {
   char userName[256];
   char date[256];
   char menuChoice = ' ';
   ShoppingCart cart;
   cart.cartSize = 0;
   printf("Enter Customer's Name:\n");
   fgets(userName,256,stdin);
   printf("Enter Today's Date:\n");
   fgets(date,256,stdin);
   userName[strlen(userName)-1] = '\0';
   printf("\n");
   printf("Customer Name: %s\n",userName);
   printf("Today's Date: %s\n", date);
   while (menuChoice != 'q') {
      menuChoice = PrintMenu(userName, date, &cart);
   }
   
   return 0;
}

