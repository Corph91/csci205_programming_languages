#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include "Deck.h"
#include "Card.h"
#include "Player.h"

void run();
void dealerAndPlayer(vector<Player>& players);
void fillDeck(vector<Card>& deckIn);
void setCard(Card& card, int val);
void shuffle(vector<Card>& deckIn, int cycles);
void deal(Deck& deckIn, Player& playerIn, bool initDeal);
int playGame(Deck& deckIn, vector<Player>& players);

int main(){

  char userOpt = ' ';
  
  while(userOpt != 'n'){
    userOpt = ' ';

    cout<<"Play game? (y/n): ";
    cin>>userOpt;
    if(userOpt == 'y'){
      run();
      cout<<endl;
    }
  }

  cout<<"Goodbye."<<endl;

  return 0;
}

void run(){
  
  Deck deck;
  deck.newDeck();
  vector<Player> players;
  players.resize(4);
 
  dealerAndPlayer(players);
  fillDeck(deck.mydeck);
  shuffle(deck.mydeck,5);

  for(int i=0; i<players.size(); i++){
    deal(deck, players.at(i), true);
  }
 
  int winner = playGame(deck, players);
  Deck deck2;
  deck2.newDeck();
  
  deck = deck2;
  
  return;
}

void dealerAndPlayer(vector<Player>& players){
  srand(time(0));
  int player = 0;
  int dealer = 0;

  while(player == dealer){
    player = rand()%4;
    dealer = rand()%4;
  }
  
  players.at(player).player = true;
  players.at(dealer).dealer = true;
  cout<<"You are player: "<<player+1<<endl<<endl;
}

void fillDeck(vector<Card>& deckIn){
  for(int i=0; i<deckIn.size(); i++){
    if(i<13){
      deckIn.at(i).suit = "Hearts";
    }
    else if(i>=13 && i<26){
      deckIn.at(i).suit = "Diamonds";
    }
    else if(i>=26 && i<39){
      deckIn.at(i).suit = "Clubs";
    }
    else{
      deckIn.at(i).suit = "Spades";
    }
    setCard(deckIn.at(i), i);
  }

  return;
}

void setCard(Card& cardIn, int val){
  val = val%13;
  switch(val){
    case 0:
      cardIn.name = "Two";
      cardIn.value = 2;
      break;
    case 1:
      cardIn.name = "Three";
      cardIn.value = 3;
      break;
    case 2:
      cardIn.name = "Four";
      cardIn.value = 4;
      break;
    case 3:
      cardIn.name = "Five";
      cardIn.value = 5;
      break;
    case 4:
      cardIn.name = "Six";
      cardIn.value = 6;
      break;
    case 5:
      cardIn.name = "Seven";
      cardIn.value = 7;
      break;
    case 6:
      cardIn.name = "Eight";
      cardIn.value = 8;
      break;
    case 7:
      cardIn.name = "Nine";
      cardIn.value = 9;
      break;
    case 8:
      cardIn.name = "Ten";
      cardIn.value = 10;
      break;
    case 9:
      cardIn.name = "Jack";
      cardIn.value = 10;
      break;
    case 10:
      cardIn.name = "Queen";
      cardIn.value = 10;
      break;
    case 11:
      cardIn.name = "King";
      cardIn.value = 10;
      break;
    case 12:
      cardIn.name = "Ace";
      cardIn.value = 11;
      break;
    default:
      cout<<"Invalid card value."<<endl;
      break;
  }

  return;
}

void shuffle(vector<Card>& deckIn, int cycles){

  while(cycles > 0){
    srand(time(0));

    for(int i=deckIn.size()-1; i>0; i--){
      int j = rand()%i;

      Card temp = deckIn.at(i);

      if(i != j){
        deckIn.at(i) = deckIn.at(j);
        deckIn.at(j) = temp;
      }
    }
    cycles--;
  }

  return;
}

void deal(Deck& deckIn, Player& playerIn, bool initDeal){

  Card card1 = deckIn.dealCard();

  if(initDeal){
    playerIn.score = card1.value;

    Card card2 = deckIn.dealCard();
    playerIn.score += card2.value;
    playerIn.hand.push_back(card2);
    playerIn.bust = false;
  }
  else{
    if(card1.name == "Ace" && (playerIn.score + card1.value) > 21){
      card1.value = 1;
    }
    playerIn.score += card1.value;
  }

  playerIn.hand.push_back(card1);

  return;
}

int playGame(Deck& deckIn, vector<Player>& players){
  int numBust = 0;
  int winningPlayer = 0;
  bool blackJack = false;

  while(numBust < players.size()-1 && winningPlayer==0 && !blackJack){
    for(int i=0; i<players.size(); i++){
      if(!players.at(i).bust){
        if(!players.at(i).player){
	  deal(deckIn, players.at(i), false);
        }
        else if(players.at(i).player && players.at(i).score != 21){
	  char playOpt = ' ';
	  cout<<"The dealer shows his hand: "<<endl;
	  for(int i=0; i<players.size(); i++){
	    if(players.at(i).dealer){
	      for(int j=0; j<players.at(i).hand.size(); j++){
		cout<<players.at(i).hand.at(j).name<<" of "<<players.at(i).hand.at(j).suit<<endl;
		cout<<"Value: "<<players.at(i).hand.at(j).value<<endl;
	      }
	    }
	  }
	  cout<<endl<<"Would you like to draw or stay [y/n] (Current score is: "<<players.at(i).score<<"): ";
	  cin>>playOpt;
	  if(playOpt == 'y'){
	    deal(deckIn, players.at(i), false);
          }
	  cout<<endl;
        }
	if(players.at(i).score > 21 && numBust < players.size()-1){
	  players.at(i).bust = true;
          numBust += 1;
        }
        else if(players.at(i).score == 21 && players.at(i).hand.size() == 2){
	  if(!players.at(i).player){
	    cout<<"Blackjack for player: "<<i+1<<endl;
	  }
	  else{
	    cout<<"You hit blackjack!"<<endl;
	  }
          winningPlayer = i+1;
	  blackJack = true;
	  break;
        }
	else if(players.at(i).score == 21 && players.at(i).hand.size() > 2){
	  if(!players.at(i).player){
	    cout<<"Player: "<<i+1<<" hits 21 and wins!"<<endl;
	  }
	  else{
	    cout<<"You hit 21 and win!"<<endl;
	  }
          winningPlayer = i+1;
	  blackJack = true;
	  break;
        }
      }
    }
  }

  if(numBust == players.size()-1 && !blackJack){
    for(int i=0; i<players.size(); i++){
      if(!players.at(i).bust){
	if(!players.at(i).player){
	  cout<<"Winner by default player: "<<i+1<<endl;
	}
	else{
	  cout<<"You win by default!"<<endl;
	}	
        winningPlayer = i+1;
      }
    }
  }

  return winningPlayer;
}

