#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    srand((int)time(0));
    char firstName[50];
    char lastName[50];
    char fullName[150];
    int die = 0;
    int sum = 0;

    printf("Please enter your first name: ");
    scanf("%s", firstName);
    printf("Please enter your last name: ");
    scanf("%s", lastName);
    
    sprintf(fullName, "%s %s",firstName, lastName);

    printf("\nHello %s! You rolled:\n", fullName);

    for(int i = 1; i <6; i++){
	die = (rand()%6)+1;
	sum += die;
        printf("Die %d is a: %d\n",i,die);
    } 

    printf("\nThe sum total of your rolls is: %d\n", sum);    

    return 0;
}
