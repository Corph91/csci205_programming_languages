#include <stdio.h>

int main(){

    int time;    
    int hours;
    int minutes;
    int seconds;       

    printf("Enter duration of time in seconds: ");
    scanf("%d", &time);
    
    hours = time/3600;
    minutes = (time - (hours * 3600))/60;
    seconds = time - (hours * 3600) - (minutes * 60);

    printf("\nThat is %d hours, %d minutes, and %d seconds.\n", hours, minutes, seconds);


    return 0;
}
