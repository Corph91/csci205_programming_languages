#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]){
    char *line = NULL;
    size_t len = 0;
    char ch;
    ssize_t read;
    char fileName[100];
    int* scores = malloc(4 * sizeof(int));
    int par = 0;
    FILE *file;
    int hole = 0;

    for(int i=0; i<4; i++){
	scores[i] = 0;
    }

    sscanf(argv[1],"%s %[^ /t]",fileName,&ch);
    file = fopen(fileName,"r");
   
    if(file == NULL){
	exit(EXIT_FAILURE);
    }

    while((read = getline(&line, &len, file)) != -1){
	hole ++;
        int tempPar;
	int* tempScores = malloc(4 * sizeof(int));

	sscanf(line, "%d\t%d %d %d %d", &tempPar, &tempScores[0], &tempScores[1], &tempScores[2], &tempScores[3]);
	
	par += tempPar;

	for(int i=0; i<4; i++){
	    scores[i] += tempScores[i];
        }
        
    }

    printf("Course Par: %d:\n", par);
    
    int min = scores[0];
    int winner = 1;
    for(int i=0; i<4; i++){
	printf("Player %d: %d\n",i+1,scores[i]);
        if(scores[i] < min){
	    min = scores[i];
	    winner = i+1;
	}
    }
    printf("The winner is Player %d!\n",winner);
       
    fclose(file);
    return 0;
}
