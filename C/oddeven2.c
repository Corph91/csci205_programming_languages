/*
 * Sean Corbett
 * 02/10/2017
 * CSCI205: Programming Languages
 * Assignment 6.14: Oddeven 2
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

    //Initial size of arrays is initialized and stored here.
    int size;
    printf("Enter the size of an array:\n");
    scanf("%d", &size);

    /* 
     * evenElms and oddElms act as size control and pointer
     * arithmetic vars, storing the number of odd and even
     * elements a user enters.
    */
    int evenElms = 0;
    int oddElms = 0;

    // Arrays for storing odd and even elements.
    int* odd = (int*)malloc(size * sizeof(int));
    int* even = (int*)malloc(size * sizeof(int));

    // Element var.
    int elm;

    /*
     * User enters vars here. If even, is stored in correct
     * memory location in even array, and evenElms counter is
     * incremented. Else is an odd element, is stored in odd
     * array, and oddElms is incremented.
     */
    for(int i=0; i<size; i++){
	printf("Enter element %d: ",i);
	scanf("%d", &elm);

	if(elm%2==0){
	    *(even + evenElms) = elm;
	    evenElms += 1;
	}
	else{
	    *(odd + oddElms) = elm;
	    oddElms += 1;
	}
    }

    // After user has entered all elements, reallocate memory
    // for odd and even array so unused space is freed.
    even = realloc(even,evenElms * sizeof(int));
    odd = realloc(odd, oddElms * sizeof(int));

    // Print out even elements from even array.
    printf("\nEven elements are:\n");
    for(int i=0; i<evenElms; i++){
	printf("%d\n",*(even + i));
    }

    // Print out odd elements from odd array.
    printf("\nOdd elements are:\n");
    for(int i=0; i<oddElms; i++){
	printf("%d\n",*(odd + i));
    }

    // Free memory held by both arrays.
    free(even);
    free(odd);

    return 0;
}
