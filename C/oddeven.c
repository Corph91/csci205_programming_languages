#include <stdio.h>
#include <stdbool.h>

int main() {
   int size;
    _Bool run = true;
   
   /* Type your code here. */
    while(run){

	printf("Enter the size of an array that can hold up to 20 integers:\n");
	scanf("%d",&size);

	if(size <=20){

	int arry[size];

	printf("Enter the elements of the array:\n");
	for(int i=0; i<size; i++){
	printf("Enter element %d:\n", i+1);
	scanf("%d", &arry[i]);
	}

	printf("The odd elements of the array are:\n");
	for(int i=0; i<size; i++){
	if(arry[i]%2 != 0){
	 printf("%d\n", arry[i]);
	}
	}

	printf("The even elements of the array are:\n");
	for(int i=0; i<size; i++){
	if(arry[i]%2 == 0){
	 printf("%d\n", arry[i]);
	}
	}

	break;

	}
	else{

	printf("Error: Enter a smaller size\n");
	break;

	}

    }
    return 0;
}
