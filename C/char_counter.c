#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>


int main(){

    int size = 100;
    char* userString = (char*)malloc(size * sizeof(char));
    char *ptr;
    int vowelCount = 0;
    int numCount = 0;
    int spaceCount = 0;
    int constCount = 0;
    int i = 0;
    char vowels[5] = {'a','e','i','o','u'};

    ptr = userString;
    printf("Enter your string:\n");
    scanf("%[^\n]%*c", ptr);
    
    while(*userString != '\0'){
	if(isalpha(*userString)){
	    for(int i=0; i<5; i++){
		if(*userString==vowels[i]){
		    vowelCount++;
		    break;
		}
		else if((i==4) && *userString != vowels[i]){
		    constCount++;
		}
	    }
        }
	else if(isdigit(*userString)){
	    numCount++;
	}
	else if(isspace(*userString)){
	    spaceCount++;
	}
	
	userString++;
    }

    printf("\nNumber of vowels: %d\n",vowelCount);
    printf("Number of consonants: %d\n",constCount);
    printf("Number of digits: %d\n", numCount);
    printf("Number of spaces: %d\n", spaceCount);

    userString = ptr;
    free(userString);
    return 0;
}

