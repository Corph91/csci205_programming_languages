#include <iostream>
using namespace std;
#include "Card.h"

void Card::setName(string cardName){
    this->name = cardName;
    return;
}

void Card::setSuit(string cardSuit){
    this->suit = cardSuit;
    return;
}

void Card::setValue(int cardValue){
   this->value = cardValue;
   return;
}

string Card::getName(){
    return this->name;
}

string Card::getSuit(){
   return this->suit;
}

int Card::getValue(){
   return this->value;
}

void Card::printCard(){
   cout<<this->name<<" of "<<this->suit<<", value: "<<this->value<<endl;
   return;
}

Card::Card(){
   this->name = "No Name";
   this->suit = "No Suit";
   this->value = 0;
}

Card::Card(string cardName, string cardSuit, int cardValue){
   this->name = cardName;
   this->suit = cardSuit;
   this->value = cardValue;
}
