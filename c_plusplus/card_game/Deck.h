#ifndef DECK_H
#define DECK_H

#include "Card.h"
#include <vector>
using namespace std;

class Deck{
  public:
    vector<Card> mydeck;
    int deckSize();
    void newDeck();
    Card dealCard();
};

#endif
