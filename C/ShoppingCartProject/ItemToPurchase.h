#ifndef ITEM_TO_PURCHASE_H
#define ITEM_TO_PURCHASE_H

typedef struct ItemToPurchase_struct {
	char itemName[100];
	int itemPrice;
	int itemQuantity;
	char itemDescription[256];
} ItemToPurchase;

void MakeItemBlank(ItemToPurchase* item);

void PrintItemCost(ItemToPurchase item);

void PrintItemDescription(ItemToPurchase item);

#endif
