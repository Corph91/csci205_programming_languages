#include <iostream>
using namespace std;

#include "Card.h"
#include "Deck.h"
#include <ctime>
#include <cstdlib>
#include <vector>

int Deck::deckSize(){
  int size = (int)this->mydeck.size();
  return size;
}

void Deck::newDeck(){
  this->mydeck.resize(52);
  return;
}

Card Deck::dealCard(){
  srand (time(0));
  int size = this->mydeck.size() - 1;
  int card = rand()%size;
  Card myCard = this->mydeck.at(card);
  this->mydeck.erase(this->mydeck.begin() + card);
  return myCard;
}


