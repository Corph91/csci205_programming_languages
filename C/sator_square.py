

def load_words(words):
	with open('words.txt','r+') as word_file:
		for line in word_file:
			line = line.split()
			for word in line:
				words.append(line)
		word_file.close()
	return words

def filter_words(words, normal_words, pallindromes):
	for word in words:
		if (word[::-1] in words) && !(word[::-1] == word):
			normal_words.append(word)
		elif (word[::-1] in words) && (word[::-1] == word):
			pallindromes.append(word)
	return normal_words, pallindromes

def main():
	words = []
	normal_words = []
	pallindromes = []
	
	
