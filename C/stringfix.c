#include<stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

int GetNumOfNonWSCharacters(const char usrStr[]) {
	int count = 0;
	for(int i=0;i<strlen(usrStr); i++){
		if(!isspace(usrStr[i])){
			count++;
		}
	}
	return count;
}

int GetNumOfWords(const char usrStr[]) {
	int count = 0;

	for(int i=0; i<strlen(usrStr); i++){
		if(isspace(usrStr[i]) || usrStr[i] == '.' || usrStr[i] == '!' || usrStr[i] == '?'){
			count++;
			i++;
		}
	}	

	return count;

}

void FixCapitalization(char usrStr[]) {
	for(int i=0;i<strlen(usrStr); i++){
		if(isupper(usrStr[0]) == 0){
			printf("I'm not upper case!\n");
			usrStr[0] = toupper(usrStr[0]);
		}
		else if((usrStr[i-2] == '.' || usrStr[i-2] == '?' || usrStr[i-2] == '!' ) && isupper(usrStr[i]) == 0){
			printf("I'm not upper either!\n");
			usrStr[i] = toupper(usrStr[i]);
		}
	}
}

void ReplaceExclamation(char usrStr[]) {
	int i,x;
	for(i=0; i< strlen(usrStr); i++){
		if(usrStr[i] == '!'){
			usrStr[i] = '.';
		}
	}      
}

void ShortenSpace(char usrStr[]) {
	int i,x;
	for(i=x=0; usrStr[i]; ++i){
		if(!isspace(usrStr[i]) || (i>0 && !isspace(usrStr[i-1]))){
			usrStr[x++] = usrStr[i];
		}
	}      
	usrStr[x] = '\0';
}


char PrintMenu(char usrStr[]) {
   char menuOp = ' ';
   
   printf("MENU\n");
   printf("c - Number of non-whitespace characters\n");
   printf("w - Number of words\n");
   printf("f - Fix capitalization\n");
   printf("r - Replace all !\'s\n");
   printf("s - Shorten spaces\n");
   printf("q - Quit\n\n");
   
   while (menuOp != 'c' && menuOp != 'w' && menuOp != 'f' &&
         menuOp != 'r' && menuOp != 's' && menuOp != 'o' && 
         menuOp != 'q') {
      printf("Choose an option:\n");
      scanf(" %c", &menuOp);
   }
   
   if (menuOp == 'c') {
      printf("Number of non-whitespace characters: %d\n\n", GetNumOfNonWSCharacters(usrStr));
      menuOp = ' ';
   }
   
   else if (menuOp == 'w') {
      printf("Number of words: %d\n\n", GetNumOfWords(usrStr));
      menuOp = ' ';
   }
   
   else if (menuOp == 'f') {
      FixCapitalization(usrStr);
      printf("Edited text: %s\n", usrStr);
      menuOp = ' ';
   }
   
   else if (menuOp == 'r') {
      ReplaceExclamation(usrStr);
      printf("Edited text: %s\n", usrStr);
      menuOp = ' ';
   }
   
   else if (menuOp == 's') {
      ShortenSpace(usrStr);
      printf("Edited text: %s\n", usrStr);
      menuOp = ' ';
   }
   
   return menuOp;
}

int main() {
   char userString[256];
   char menuChoice = ' ';

   printf("Enter a sample text:\n");
   fgets(userString, 256, stdin);
   printf("\n");
   
   printf("You entered: %s\n", userString);
   
   while (menuChoice != 'q') {
      menuChoice = PrintMenu(userString);
   }
   
   return 0;
}


